# Skabelon til Synopsis med Quarto

Dette repo indeholder en skabelon til at skrive dokumenter med [markdown](https://quarto.org/docs/authoring/markdown-basics.html) vha. [Quarto](https://quarto.org/).

Skabelonen kan bruges bredt, men er specifikt opbygget til synopsis i programmeringsfaget.

Eksterne kodefiler inkluderes vha. Quarto udvidelsen [include-code-files](https://github.com/quarto-ext/include-code-files).
